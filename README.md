
Working-repo for cryptographically secure big integers
======================================================

**DO NOT USE!** (for now)


Currently this is mostly for my personal use. The intention of this project is
to write a library for big integers that is usable as it stands in cryptography.
At the moment this is however not the case!

While big integers are useful tools in many areas, the special usage-requirements
in cryptography are often in direct opposition to what would normaly be considered
desirable. As such this library will not be very suitable for general-purpose-uses
even when it will be completed. (And then integrated into a proper crypto-library.)

Examples for problems:

* Timing-side-channels are a real threat in cryptography and as such all algorithms
  must take the same time to complete, independently of whether the data would allow
  huge speedups. (Think about equality-checks: In cryptographic use-cases you have to
  compare *all* limbs, even if the first pair is already different.)
* Negative values and by extension proper subtraction is not really needed: In
  cryptography we will almost always use residue-classes which allow us to avoid
  negative values all together.

