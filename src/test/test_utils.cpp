#include "test_utils.hpp"

#include <algorithm>
#include <cassert>

double average_deviation(std::vector<u32> lhs, std::vector<u32> rhs) {
	assert(lhs.size() == rhs.size());
	std::sort(lhs.begin(), lhs.end());
	std::sort(rhs.begin(), rhs.end());
	const auto n = lhs.size();
	auto ratio_sum = 0.0;
	for (auto i = std::size_t{}; i < n; ++i) {
		if (lhs[i] >= rhs[i]) {
			ratio_sum += ((1.0 * lhs[i]) /rhs[i]) - 1.0;
		} else {
			ratio_sum -= ((1.0 * rhs[i]) /lhs[i]) - 1.0;
		}
	}
	return ratio_sum / static_cast<double>(n);
}
