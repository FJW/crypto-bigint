#include <catch.hpp>

#include <bigint/bigint.hpp>

using namespace bigint;

using vec = std::vector<u32>;

TEST_CASE("vector-ctor", "[bigint]") {

	auto i = bigint::bigint{vec{1, 2}};
	CHECK(i.get_view().size() == 2);
}

TEST_CASE("literals", "[bigint]") {
	CHECK(0x0_bi == bigint::bigint{vec{0}});
	CHECK(0x1_bi == bigint::bigint{vec{1}});
	CHECK(0x1234'5678_bi == bigint::bigint{vec{0x1234'5678u}});
	CHECK(0x1234'5678'90ab'cdef'0123'4567'89ab'cdef_bi ==
	      (bigint::bigint{vec{0x89ab'cdefu, 0x0123'4567u, 0x90ab'cdefu, 0x1234'5678u}}));
}

TEST_CASE("equality", "[bigint]") {
	const auto i0 = bigint::bigint{vec{0}};
	const auto i1 = bigint::bigint{vec{1, 2}};
	const auto i2 = bigint::bigint{vec{1, 2}};
	CHECK(not(i0 == i1));
	CHECK(not(i0 == i2));
	CHECK(i1 == i2);
	CHECK(i0 != i1);
	CHECK(i0 != i2);
	CHECK(not(i1 != i2));
}

TEST_CASE("comparisson", "[bigint]") {
	const auto i0 = bigint::bigint{vec{0}};
	const auto i1 = bigint::bigint{vec{1, 2}};
	CHECK(i0 < i1);
	const auto i2 = bigint::bigint{{5, 1, 0, 0}};
	CHECK(i1 > i2);
	const auto i3 = bigint::bigint{vec{1u}};
	CHECK(i3 > i0);
	const auto i4 = bigint::bigint{vec{1, 2, 3, 4, 5, 6, 7, 8}};
	const auto i5 = bigint::bigint{vec{2, 3, 4, 5, 6, 7, 8, 1}};
	CHECK(i4 > i5);
	CHECK(i4 <= i4);
	CHECK(i4 >= i4);
	const auto i6 = bigint::bigint{vec{0xf000'0001u}};
	const auto i7 = bigint::bigint{vec{0x1000'0004u}};
	CHECK(i6 > i7);
}

TEST_CASE("addition", "[bigint]") {
	auto i0 = bigint::bigint{vec{0, 0}};
	auto i1 = bigint::bigint{vec{1, 2}};
	const auto i2 = bigint::bigint{vec{1, 2, 0}};
	CHECK(i0 != i1);
	i0 += i1;
	CHECK(i0 == i2);
	i1 += i0;
	CHECK(i1 == bigint::bigint(vec{2, 4, 0}));
	const auto i3 = bigint::bigint{vec{0xffff'ffffu, 0xffff'ffffu, 0xffff'ffffu}};
	const auto i4 = bigint::bigint{vec{3}};
	CHECK(i3 + i4 == bigint::bigint(vec{2, 0, 0, 1}));
}

TEST_CASE("subtraction", "[bigint]") {
	const auto i0 = bigint::bigint{vec{0, 0}};
	const auto i1 = bigint::bigint{vec{1, 2}};
	const auto i2 = i1 - i0;
	CHECK(i1 == i2);
	const auto i3 = i2 - i1;
	CHECK(i3 == i0);
	const auto i4 = bigint::bigint{vec{2, 0, 0, 1}};
	const auto i5 = bigint::bigint{vec{3}};
	const auto i6 = bigint::bigint{vec{0xffff'ffffu, 0xffff'ffffu, 0xffff'ffffu}};
	CHECK(i4 - i5 == i6);
	const auto i7 = bigint::bigint{vec{5, 1}};
	const auto i8 = bigint::bigint{vec{0xf000'0001u}};
	const auto i9 = bigint::bigint{vec{0x1000'0004u}};
	CHECK(i7 - i8 == i9);
	auto i10 = bigint::bigint{vec{2, 0, 0, 1}};
	i10 -= i1;
	CHECK(i10 == (bigint::bigint{vec{1, 0xffff'fffe, 0xffff'ffff}}));
	i10 = bigint::bigint{vec{2, 3}};
	i10 -= i1;
	CHECK(i10 == (bigint::bigint{vec{1, 1}}));
}

TEST_CASE("multiplication", "[bigint]") {
	CHECK(0x563faa895e52ea67b12cca71704692ec_bi * 0xbc23940fcb60beff007d4c42bc43de5_bi ==
	      0x3f62bdd352e44682b41ff4e2891c73ea7060ade42750a18e22bdca060d3a91c_bi);
	auto tmp = 0x7cc17b2195c0aec58911ffcdccea73bf_bi;
	tmp *= 0xfb9020282ce7091b4d250c05fda807f8_bi;
	CHECK(tmp == 0x7a97f03af4396fe574d5698072cbaaa1c2ed2cd4c6672732b59cd4f3a4a25a08_bi);
	tmp *= 0x0_bi;
	CHECK(tmp == 0x0_bi);
}

TEST_CASE("modulo", "[bigint]") {
	auto i = bigint::bigint{{5, 1}};
	CHECK((i % bigint::bigint{vec{0x8000'0000u}}) == (bigint::bigint{vec{5}}));
	CHECK((i % bigint::bigint{vec{0xf000'0001u}}) == (bigint::bigint{vec{0x1000'0004}}));
	i = bigint::bigint{vec{5, 4, 3, 2}};
	CHECK((i % bigint::bigint{vec{0x8000'0000u}}) == (bigint::bigint{vec{5}}));
	CHECK((i % bigint::bigint{vec{23, 0xff00'0042}}) ==
	      (bigint::bigint{vec{0xd1d1'dd9cu, 0xfa7b'9d17u}}));
	i %= bigint::bigint{vec{0, 0x8000'0000}};
	CHECK(i == (bigint::bigint{vec{5, 4}}));
	CHECK(i.get_view().size() == 2);
}

TEST_CASE("leftshift", "[bigint]") {
	auto i = bigint::bigint{vec{5}};
	CHECK((i <<= 1) == (bigint::bigint{vec{10}}));
	CHECK((i <<= 3) == (bigint::bigint{vec{80}}));
	CHECK((i <<= 32) == (bigint::bigint{vec{0, 80}}));
	CHECK((i <<= 33) == (bigint::bigint{vec{0, 0, 160}}));
	CHECK((i << 2) == (bigint::bigint{vec{0, 0, 640}}));
	CHECK(i == (bigint::bigint{vec{0, 0, 160}}));
	i = bigint::bigint{vec{0xffff'ffffu}};
	CHECK((i <<= 32) == (bigint::bigint{vec{0, 0xffff'ffffu}}));
}

TEST_CASE("division", "[bigint]") {
	CHECK(0x23_bi / 0x5_bi == 0x7_bi);
	CHECK(0x9b62a02d817ff48299246d0dc61db721_bi / 0x169c9dd751f0d54f1dcd5929ea_bi ==
	      0x6df32ee_bi);
	CHECK(0xcdf4b166b2e4c170ec2ea8356d6b96d76fa807e92d74f96fce0809f016331843_bi /
	              0xf5b487f9b175d51314531d28ad9b151f_bi ==
	      0xd695cdf0f3e45c686f6fa76172ed496c_bi);

	auto i0 = 0xf02d1cfd1bba2f57de54197a522fee3b6f7c5ddbdfaa30daddf041798821f246_bi;
	i0 /= 0xe29d02f6b0039b5a56b119bec84333df_bi;
	CHECK(i0 == 0x10f525b015c500c816c337149671b162d_bi);
}

TEST_CASE("power", "[bigint]") {
	CHECK(pow(0x123_bi, 0x456_bi, 0x789_bi) == 0x306_bi);
	CHECK(pow(0x2_bi, 0x80_bi, 0x1124f_bi) == 0xf73d_bi);

	const auto base = 0xcefbf1737cd8ed2bff610e40cb7d0aee98a59bee7d30a57d3485cd5cccf7c2ba_bi;
	const auto exp = 0xc9f7d99d3361af6baed9d6ebff42b7541c3a2e081cf6512cc7ac39e663c3baba_bi;
	const auto mod = 0xd7080ab59ad9fab408f5312ae8fc00b422a3e9e19c733a33f1e8cad861ae9c3e_bi;
	const auto expected = 0x1185de754ae1c26c3751913e707f95a772042e7dc85c89625cb6608449cf5eb6_bi;
	CHECK(pow(base, exp, mod) == expected);
}
