#include <catch.hpp>

#include <bigint/parsing.hpp>

TEST_CASE("count_limbs", "[bigint][parsing]") {
	CHECK(bigint::count_limbs("") == std::nullopt);
	CHECK(bigint::count_limbs("0x") == std::nullopt);
	CHECK(bigint::count_limbs("0x'") == std::nullopt);
	CHECK(bigint::count_limbs("123") == std::nullopt);
	CHECK(bigint::count_limbs("0x1") == std::optional{1u});
	CHECK(bigint::count_limbs("0x12") == std::optional{1u});
	CHECK(bigint::count_limbs("0x1234") == std::optional{1u});
	CHECK(bigint::count_limbs("0x12345678") == std::optional{1u});
	CHECK(bigint::count_limbs("0xffffffff'ffffffff") == std::optional{2u});
	CHECK(bigint::count_limbs("0x1'ffffffff'ffffffff") == std::optional{3u});
}

TEST_CASE("parse_bigint", "[bigint][parsing]") {
	auto out = std::vector<bigint::u32>(2);
	bigint::parse_bigint(out, "0x0");
	CHECK(out == (std::vector<bigint::u32>{0,0}));
	bigint::parse_bigint(out, "0x1");
	CHECK(out == (std::vector<bigint::u32>{1,0}));
	bigint::parse_bigint(out, "0xffff'ffff");
	CHECK(out == (std::vector<bigint::u32>{0xffff'ffffu,0}));
	bigint::parse_bigint(out, "0xffff'ffff'ffff");
	CHECK(out == (std::vector<bigint::u32>{0xffff'ffffu,0xffffu}));
	bigint::parse_bigint(out, "0x1234'5678'9abc");
	CHECK(out == (std::vector<bigint::u32>{0x5678'9abcu,0x1234u}));

	out = std::vector<bigint::u32>(4);
	bigint::parse_bigint(out, "0x1234'5678'9abc'def0'1357'9be2'468a");
	CHECK(out == (std::vector<bigint::u32>{0x9be2'468au,0xdef0'1357u,0x5678'9abcu, 0x1234u}));
}
