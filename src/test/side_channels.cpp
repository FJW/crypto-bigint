#include <catch.hpp>

#include <algorithm>
#include <cmath>
#include <numeric>
#include <vector>

#include <bigint/arithmetics.hpp>

#include "test_utils.hpp"

using namespace bigint;

// Note that this test CAN fail without real reason from time to time
// as an average performance ratio between 0.99 and 1.01 is a rather
// strong requirement. If it fails repeatedly however, especially with
// a clear preference for the direction, that would be indicative of
// a problem.
//
// Also note that this test is quite slow in debug-builds; as release-builds
// are however the interessting thing to test with these, this shouldn't
// be a real issue.
//
// Finally note that for all of the above reason, this test is not enabled
// by default; to run it you need to pass `[timing]` or a similar CLI-argument
// to the unittests.
TEST_CASE("timing-safety of compare", "[.][bigint][compare][timing]") {
	const auto samples = 100u;
	auto vec0 = std::vector<u32>(128 * 1024);
	auto vec1 = std::vector<u32>(128 * 1024);
	auto vec2 = std::vector<u32>(128 * 1024);
	std::iota(vec0.begin(), vec0.end(), 0);
	std::iota(vec1.begin(), vec1.end(), 0);
	std::iota(vec2.begin(), vec2.end(), 1);
	const auto avg_deviation =
	        compare_runtime([&] { return compare(limb_view{vec0}, limb_view{vec1}); },
	                        [&] { return compare(limb_view{vec1}, limb_view{vec2}); }, samples);
	CHECK(std::abs(avg_deviation) < 0.01);
}
