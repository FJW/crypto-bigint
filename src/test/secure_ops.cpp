#include <catch.hpp>

#include <cmath>
#include <random>
#include <vector>

#include <bigint/secure_ops.hpp>

#include "test_utils.hpp"

using bigint::u32;

TEST_CASE("select u32 (correctness)", "[secure-ops]") {
	CHECK(bigint::select(23u, 42u, false) == 23u);
	CHECK(bigint::select(23u, 42u, true) == 42u);
}

TEST_CASE("select limb_view (correctness)", "[secure-ops]") {
	const auto lhs = std::vector<u32>{1, 2, 3};
	const auto rhs = std::vector<u32>{4, 5, 6};
	auto out = std::vector<u32>(3);
	bigint::select(out, lhs, rhs, false);
	CHECK(out == lhs);
	bigint::select(out, lhs, rhs, true);
	CHECK(out == rhs);
}

TEST_CASE("select u32 (timing-safety)", "[.][secure-ops][timing]") {
	const auto sample_size = 1000000u;
	struct bool_wrapper {
		bool b;
	};
	auto vec0 = std::vector<bool_wrapper>{};
	auto vec1 = std::vector<bool_wrapper>{};
	auto vec2 = std::vector<bool_wrapper>{};
	auto rng = std::default_random_engine{std::random_device{}()};
	for (auto i = 0u; i < sample_size; ++i) {
		vec0.push_back({(rng() | 1u) == 1u});
		vec1.push_back({true});
		vec2.push_back({false});
	}
	{
		auto i0 = 0u;
		auto i1 = 0u;
		const auto avg_deviation = compare_runtime(
		        [&] { return bigint::select(23, 42, vec0[i0++].b); },
		        [&] { return bigint::select(23, 42, vec1[i1++].b); }, sample_size);
		CHECK(std::abs(avg_deviation) < 0.01);
	}
	{
		auto i0 = 0u;
		auto i1 = 0u;
		const auto avg_deviation = compare_runtime(
		        [&] { return bigint::select(23, 42, vec0[i0++].b); },
		        [&] { return bigint::select(23, 42, vec2[i1++].b); }, sample_size);
		CHECK(std::abs(avg_deviation) < 0.01);
	}
	{
		auto i0 = 0u;
		auto i1 = 0u;
		const auto avg_deviation = compare_runtime(
		        [&] { return bigint::select(23, 42, vec1[i0++].b); },
		        [&] { return bigint::select(23, 42, vec2[i1++].b); }, sample_size);
		CHECK(std::abs(avg_deviation) < 0.01);
	}
}
