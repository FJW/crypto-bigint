#include <catch.hpp>

#include <vector>

#include <bigint/arithmetics.hpp>

#include <iostream>

using namespace bigint;

using vec_u32 = std::vector<bigint::u32>;

TEST_CASE("compare", "[bigint]") {
	const auto i0 = vec_u32{0};
	const auto i1 = vec_u32{1, 2};
	const auto i2 = vec_u32{1, 2};
	CHECK(limb_view{i0} < limb_view{i1});
	CHECK(limb_view{i0} < limb_view{i2});
	CHECK(limb_view{i1} == limb_view{i2});
	const auto i3 = vec_u32{1};
	CHECK(limb_view{i3} > limb_view{i0});
	const auto i4 = vec_u32{1, 2, 3, 4, 5, 6, 7, 8};
	const auto i5 = vec_u32{2, 3, 4, 5, 6, 7, 8, 1};
	CHECK(limb_view{i4} > limb_view{i5});
	CHECK(limb_view{i4} == limb_view{i4});
	const auto i6 = vec_u32{0xf000'0001u};
	const auto i7 = vec_u32{0x1000'0004u};
	CHECK(limb_view{i6} > limb_view{i7});
	const auto i8 = vec_u32{5, 1, 0, 0};
	CHECK(limb_view{i1} > limb_view{i8});
}

TEST_CASE("add", "[bigint]") {
	auto i0 = vec_u32{0, 0};
	auto i1 = vec_u32{1, 2};
	const auto i2 = vec_u32{1, 2, 0};
	CHECK(add(i0, i0, i1) == 0);
	CHECK(limb_view{i0} == limb_view{i2});
	CHECK(add(i1, i1, i0) == 0);
	const auto i3 = vec_u32{2, 4, 0};
	CHECK(limb_view{i1} == limb_view{i3});
	const auto i4 = vec_u32{0xffff'ffffu, 0xffff'ffffu, 0xffff'ffffu};
	const auto i5 = vec_u32{3, 0};
	const auto i6 = vec_u32{2, 0, 0, 1};
	auto i7 = vec_u32(4);
	i7[3] = add(i7, i4, i5);
	CHECK(limb_view{i7} == limb_view{i6});
}

TEST_CASE("sub", "[bigint]") {
	auto i0 = vec_u32{0, 0};
	auto i1 = vec_u32{1, 2};
	auto i2 = vec_u32{0, 0};
	CHECK(sub(i2, i1, i0) == 0);
	CHECK(limb_view{i1} == limb_view{i2});
	auto i3 = vec_u32(2);
	CHECK(sub(i3, i2, i1) == 0);
	CHECK(limb_view{i3} == limb_view{i0});
	const auto i4 = vec_u32{2, 0, 0, 1};
	const auto i5 = vec_u32{3, 0};
	const auto i6 = vec_u32{0xffff'ffffu, 0xffff'ffffu, 0xffff'ffffu};
	auto i7 = vec_u32(5);
	i7[4] = sub(i7, i4, i5);
	CHECK(limb_view{i7} == limb_view{i6});
}

TEST_CASE("sub if", "[bigint]") {
	auto i0 = vec_u32{5, 3};
	auto i1 = vec_u32{1, 2};
	auto o0 = vec_u32{0, 0};
	sub_if(o0, i0, i1, false);
	CHECK(limb_view{o0} == limb_view{i0});
	sub_if(o0, i0, i1, true);
	CHECK(limb_view{o0} == limb_view{(vec_u32{4, 1})});
}

TEST_CASE("mul", "[bigint]") {
	{
		const auto i0 = vec_u32{2};
		const auto i1 = vec_u32{3};
		const auto expected = vec_u32{6, 0};
		auto out = vec_u32(i0.size() + i1.size());
		mul(out, i0, i1);
		CHECK(limb_view{out} == limb_view{expected});
	}
	{
		const auto i0 = vec_u32{1, 2};
		const auto i1 = vec_u32{3, 4};
		const auto expected = vec_u32{3, 10, 8, 0};
		auto out = vec_u32(i0.size() + i1.size());
		mul(out, i0, i1);
		CHECK(limb_view{out} == limb_view{expected});
	}
	{
		const auto i0 = vec_u32{1, 0x8000'0000u, 3};
		const auto i1 = vec_u32{4};
		const auto expected = vec_u32{4, 0, 14, 0};
		auto out = vec_u32(i0.size() + i1.size());
		mul(out, i0, i1);
		CHECK(limb_view{out} == limb_view{expected});
	}
	{
		// randomly generated using python:
		const auto i0 = vec_u32{3803632716, 1095453543, 406336179, 3506027727};
		const auto i1 = vec_u32{3067234330, 2590286401, 1130495038, 2297100700};
		const auto expected = vec_u32{0xa09eefb8u, 0x6db4d484u, 0xae495052u, 0x07c0eb13u,
		                              0x76bffefcu, 0xac47c8c5u, 0x11ffd402u, 0x6fc47c57u};
		auto out = vec_u32(i0.size() + i1.size());
		mul(out, i0, i1);
		CHECK(limb_view{out} == limb_view{expected});
	}
}

TEST_CASE("lshift", "[bigint]") {
	const auto in0 = vec_u32{5};
	auto out = vec_u32(4);
	lshift(out, in0, 1);
	CHECK(limb_view{out} == limb_view{(vec_u32{10})});

	out = vec_u32(4);
	lshift(out, in0, 4);
	CHECK(limb_view{out} == limb_view{(vec_u32{80})});

	out = vec_u32(4);
	lshift(out, in0, 36);
	CHECK(limb_view{out} == limb_view{(vec_u32{0, 80})});

	out = vec_u32(4);
	lshift(out, in0, 69);
	CHECK(limb_view{out} == limb_view{(vec_u32{0, 0, 160})});

	const auto in1 = vec_u32{0xffff'ffffu};
	out = vec_u32(4);
	lshift(out, in1, 32);
	CHECK(limb_view{out} == limb_view{(vec_u32{0, 0xffff'ffffu})});
}

TEST_CASE("div_2", "[bigint]") {
	const auto zero = vec_u32{0};
	auto i0 = vec_u32{0};
	CHECK(i0 == zero);
	div_2(i0);
	CHECK(i0 == zero);
	auto i1 = vec_u32{3, 4};
	div_2(i1);
	CHECK(i1 == (vec_u32{1, 2}));
	div_2(i1);
	CHECK(i1 == (vec_u32{0, 1}));
	div_2(i1);
	CHECK(i1 == (vec_u32{0x8000'0000u, 0}));
}

TEST_CASE("mul_2", "[bigint]") {
	const auto zero = vec_u32{0};
	auto i0 = vec_u32{0};
	CHECK(i0 == zero);
	mul_2(i0);
	CHECK(i0 == zero);
	auto i1 = vec_u32{3, 4};
	mul_2(i1);
	CHECK(i1 == (vec_u32{6, 8}));
	mul_2(i1);
	CHECK(i1 == (vec_u32{12, 16}));
	auto i2 = vec_u32{0x8000'0001u, 1};
	mul_2(i2);
	CHECK(i2 == (vec_u32{2, 3}));
}

TEST_CASE("mod", "[bigint]") {
	{
		auto inout = vec_u32{5, 1};
		auto scratch_space = vec_u32(2);
		mod(inout, vec_u32{0x8000'0000u}, 32u, scratch_space);
		CHECK(inout == limb_view{vec_u32{5}});
	}
	{
		auto inout = vec_u32{5, 1};
		auto scratch_space = vec_u32(2);
		mod(inout, vec_u32{0xf000'0001u}, 32u, scratch_space);
		CHECK(inout == limb_view{vec_u32{0x1000'0004}});
	}
	{
		auto inout = vec_u32{5, 4, 3, 2};
		auto scratch_space = vec_u32(4);
		mod(inout, vec_u32{0x8000'0000u}, 32u, scratch_space);
		CHECK(inout == limb_view{vec_u32{5}});
	}
	{
		auto inout = vec_u32{5, 4, 3, 2};
		auto scratch_space = vec_u32(4);
		mod(inout, vec_u32{23, 0xff00'0042u}, 64u, scratch_space);
		CHECK(inout == limb_view{(vec_u32{0xd1d1'dd9cu, 0xfa7b'9d17u})});
	}
	{
		auto inout = vec_u32{1u, 2u};
		auto scratch_space = vec_u32(2);
		mod(inout, vec_u32{13u}, 4u, scratch_space);
		CHECK(inout == limb_view{vec_u32{6}});
	}
	{
		auto inout = vec_u32{1u, 200u};
		auto scratch_space = vec_u32(2);
		mod(inout, vec_u32{13u}, 4u, scratch_space);
		CHECK(inout == limb_view{vec_u32{7}});
	}
}

TEST_CASE("div_mod", "[bigint]") {
	{
		auto out = vec_u32(1);
		auto lhs = vec_u32{25};
		auto rhs = vec_u32{3};
		auto scratch_space = vec_u32(1);
		div_mod(out, lhs, rhs, scratch_space);
		CHECK(limb_view{out} == limb_view{vec_u32{25u / 3u}});
	}
	{
		auto out = vec_u32(1);
		auto lhs = vec_u32{212577};
		auto rhs = vec_u32{33480};
		auto scratch_space = vec_u32(1);
		div_mod(out, lhs, rhs, scratch_space);
		CHECK(limb_view{out} == limb_view{vec_u32{212577u / 33480u}});
	}
	{
		auto out = vec_u32(1);
		auto lhs = vec_u32{212577};
		auto rhs = vec_u32{5};
		auto scratch_space = vec_u32(1);
		div_mod(out, lhs, rhs, scratch_space);
		CHECK(limb_view{out} == limb_view{vec_u32{212577u / 5u}});
	}
	{
		auto out = vec_u32(2);
		auto lhs = vec_u32{11, 5};
		auto rhs = vec_u32{5};
		auto scratch_space = vec_u32(2);
		div_mod(out, lhs, rhs, scratch_space);
		CHECK(limb_view{out} == limb_view(vec_u32{2, 1}));
	}
	{
		auto out = vec_u32(5);
		auto lhs = vec_u32{239964318, 3169496007, 3785450561, 3719928800, 1647187751};
		auto rhs = vec_u32{3833633451, 4120057375, 3267761248};
		auto scratch_space = vec_u32(5);
		div_mod(out, lhs, rhs, scratch_space);
		CHECK(limb_view{out} == limb_view(vec_u32{51571492, 2164973811}));
		CHECK(limb_view{lhs} == limb_view(vec_u32{4248229778, 2649376092, 1510073450}));
	}
}

TEST_CASE("pow", "[bigint]") {
	{
		const auto base = vec_u32{2};
		const auto e = vec_u32{128};
		const auto m = vec_u32{70223};
		auto out = vec_u32(m.size());
		auto scratch_space = vec_u32(7u * m.size());
		pow(out, base, e, m, count_bits(m), scratch_space);
		CHECK(limb_view{out} == limb_view{vec_u32{63293}});
	}
	{
		const auto base = vec_u32{89779442u,  3310405408u, 608548063u,
		                          112545176u, 783435644u,  3161918544u};
		const auto e = vec_u32{23u};
		const auto m = vec_u32{3631153330u, 1785167714u, 528936198u,
		                       1666433335u, 1597898103u, 3333633272u};
		auto out = vec_u32(m.size());
		const auto expected =
		        vec_u32{3023826, 1725526494, 2623182989, 2434195243, 904531331, 1342260084};
		auto scratch_space = vec_u32(7u * m.size());
		pow(out, base, e, m, count_bits(m), scratch_space);
		CHECK(limb_view{out} == limb_view{expected});
	}
	{
		const auto base = vec_u32{89779442u,  3310405408u, 608548063u,
		                          112545176u, 783435644u,  3161918544u};
		const auto e = vec_u32{3811837471u, 3856313099u, 4207953816u,
		                       2389742684u, 3626593418u, 2615951072u};
		const auto m = vec_u32{3631153330u, 1785167714u, 528936198u,
		                       1666433335u, 1597898103u, 3333633272u};
		auto out = vec_u32(m.size());
		const auto expected = vec_u32{415869774u,  3516891800u, 3088902585u,
		                              4042910517u, 3773427453u, 328897476u};
		auto scratch_space = vec_u32(7u * m.size());
		pow(out, base, e, m, count_bits(m), scratch_space);
		CHECK(limb_view{out} == limb_view{expected});
	}
}
