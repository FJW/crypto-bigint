#ifndef BIGINT_TEST_UTILS_HPP
#define BIGINT_TEST_UTILS_HPP

#include <chrono>
#include <vector>

#include <bigint/core.hpp>

using bigint::u32;

class timer {
public:
	using clock = std::chrono::steady_clock;
	void start() { m_start = clock::now(); }
	void stop() { m_end = clock::now(); }

	u32 ns() const {
		return static_cast<u32>(
		        std::chrono::duration_cast<std::chrono::nanoseconds>(m_end - m_start)
		                .count());
	}

private:
	clock::time_point m_start;
	clock::time_point m_end;
};

template <typename F>
u32 time(F fun) {
	auto t = timer{};
	t.start();
	const auto res = fun();
	t.stop();
	[[maybe_unused]] volatile const auto res2 = res;
	return t.ns();
}

double average_deviation(std::vector<u32> lhs, std::vector<u32> rhs);

template <typename F0, typename F1>
double compare_runtime(F0 f0, F1 f1, unsigned n) {
	auto times_0 = std::vector<u32>{};
	auto times_1 = std::vector<u32>{};
	for (auto i = 0u; i < n; ++i) {
		times_0.push_back(time(f0));
		times_1.push_back(time(f1));
	}
	return average_deviation(std::move(times_0), std::move(times_1));
}

#endif
