#include <catch.hpp>

#include <vector>

#include <bigint/range.hpp>

using namespace bigint;

using vec_u32 = std::vector<bigint::u32>;

TEST_CASE("count-bits", "[bigint]") {
	CHECK(count_bits(vec_u32{0}) == 0u);
	CHECK(count_bits(vec_u32{0,0}) == 0u);
	CHECK(count_bits(vec_u32{3}) == 2u);
	CHECK(count_bits(vec_u32{3,0}) == 2u);
	CHECK(count_bits(vec_u32{0,3,0}) == 34u);
	CHECK(count_bits(vec_u32{3,1}) == 33u);
}
