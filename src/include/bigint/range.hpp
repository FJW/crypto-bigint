#ifndef BIGINT_RANGE_HPP
#define BIGINT_RANGE_HPP

#include <array>
#include <cassert>
#include <iosfwd>
#include <numeric>
#include <vector>

#include "core.hpp"

namespace bigint {

class limb_span {
public:
	limb_span(u32* ptr, std::size_t len) : m_ptr{ptr}, m_len{len} { assert(ptr != nullptr); }
	limb_span(std::vector<u32>& vec) : m_ptr{vec.data()}, m_len{vec.size()} {}

	u32 operator[](std::size_t i) const {
		assert(i < m_len);
		return m_ptr[i];
	}

	u32& operator[](std::size_t i) {
		assert(i < m_len);
		return m_ptr[i];
	}

	u32* begin() { return m_ptr; }
	const u32* begin() const { return m_ptr; }
	u32* end() { return m_ptr + m_len; }
	const u32* end() const { return m_ptr + m_len; }

	std::size_t size() const { return m_len; }

	u32* data() { return m_ptr; }
	const u32* data() const { return m_ptr; }

	bool get_bit(std::size_t i) const {
		assert(i / 32u < m_len);
		const auto bit = i % 32u;
		return static_cast<bool>((m_ptr[i / 32u] >> bit) bitand 1u);
	}

	limb_span splice(std::size_t len) const {
		assert(len <= m_len);
		return {m_ptr, len};
	}

private:
	u32* m_ptr;
	std::size_t m_len;
};

class limb_view {
public:
	limb_view(const u32* ptr, std::size_t len) : m_ptr{ptr}, m_len{len} {
		assert(ptr != nullptr);
	}

	limb_view(limb_span s) : m_ptr{s.data()}, m_len{s.size()} {}

	limb_view(const std::vector<u32>& vec) : m_ptr{vec.data()}, m_len{vec.size()} {}

	u32 operator[](std::size_t i) const {
		assert(i < m_len);
		return m_ptr[i];
	}

	const u32* begin() const { return m_ptr; }
	const u32* end() const { return m_ptr + m_len; }

	std::size_t size() const { return m_len; }

	bool get_bit(std::size_t i) const {
		assert(i / 32u < m_len);
		const auto bit = i % 32u;
		return static_cast<bool>((m_ptr[i / 32u] >> bit) bitand 1u);
	}

private:
	const u32* m_ptr;
	std::size_t m_len;
};

std::ostream& operator<<(std::ostream& out, limb_view v);

inline std::ostream& operator<<(std::ostream& out, limb_span v) { return out << limb_view{v}; }

std::size_t count_bits(limb_view);
inline std::size_t count_bits(limb_span ls) { return count_bits(limb_view{ls}); }

template <typename... Integer>
std::array<limb_span, sizeof...(Integer)> split(limb_span in, Integer... sizes) {
	assert((sizes + ...) <= in.size());
	constexpr auto dimension = sizeof...(Integer);
	auto sizes_array = std::array<std::size_t, dimension>{sizes...};
	// In the following line we have to create `dimension` limb_spans which
	// are non default-construcible; for this we have to create `dimension` expressions
	// using the ...-syntax. Since we don't need any particular from the sizes however,
	// we discard those values and just return an empty span instead.
	auto ret = std::array<limb_span, dimension>{([&in](auto) {
		return limb_span{in.begin(), 0};
	}(sizes))...};
	auto base_ptr = in.begin();
	for (auto i = std::size_t{}; i < dimension; ++i) {
		const auto size = sizes_array[i];
		ret[i] = {base_ptr, size};
		base_ptr += size;
	}
	return ret;
}

template <typename... Integer>
std::array<limb_view, sizeof...(Integer)> split(limb_view in, Integer... sizes) {
	assert((sizes + ...) <= in.size());
	constexpr auto dimension = sizeof...(Integer);
	auto sizes_array = std::array<std::size_t, dimension>{sizes...};
	// for an explanation see the one in `split(limb_span, ...)`:
	auto ret = std::array<limb_view, dimension>{([&in](auto) {
		return limb_view{in.begin(), 0};
	}(sizes))...};
	auto base_ptr = in.begin();
	for (auto i = std::size_t{}; i < dimension; ++i) {
		const auto size = sizes_array[i];
		ret[i] = {base_ptr, size};
		base_ptr += size;
	}
	return ret;
}

} // namespace bigint

#endif
