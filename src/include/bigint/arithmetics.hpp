#ifndef BIGINT_ARITHMETICS_HPP
#define BIGINT_ARITHMETICS_HPP

#include <cstdint>
#include <ostream>

#include "core.hpp"
#include "range.hpp"

namespace bigint {

enum class order : std::uint8_t { less = 0b01, equal = 0b10, greater = 0b00 };

std::ostream& operator<<(std::ostream& out, order o);

order compare(limb_view lhs, limb_view rhs);

inline bool operator==(limb_view lhs, limb_view rhs) noexcept {
	return compare(lhs, rhs) == order::equal;
}
inline bool operator<(limb_view lhs, limb_view rhs) noexcept {
	return compare(lhs, rhs) == order::less;
}
inline bool operator!=(limb_view lhs, limb_view rhs) noexcept { return not(lhs == rhs); }
inline bool operator<=(limb_view lhs, limb_view rhs) noexcept { return not(rhs < lhs); }
inline bool operator>(limb_view lhs, limb_view rhs) noexcept { return rhs < lhs; }
inline bool operator>=(limb_view lhs, limb_view rhs) noexcept { return not(lhs < rhs); }

// out may overlap lhs/in, as long as they share their
// least significant limb:
u32 add(limb_span out, limb_view lhs, limb_view rhs);
u32 sub(limb_span out, limb_view lhs, limb_view rhs);
void sub_if(limb_span out, limb_view lhs, limb_view rhs, bool b);

void lshift(limb_span out, limb_view in, std::size_t n);
void div_2(limb_span inout);
bool mul_2(limb_span inout);

void mul(limb_span out, limb_view lhs, limb_view rhs);
void mod(limb_span inout, limb_view m, std::size_t m_bits, limb_span scratch);
void div_mod(limb_span div_out, limb_span inout, limb_view rhs, limb_span scratch);

void pow(limb_span out, limb_view base, limb_view exp, limb_view m, std::size_t mod_bits,
         limb_span scratch);

} // namespace bigint

#endif
