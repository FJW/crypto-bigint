#ifndef BIGINT_SECURE_OPS_HPP
#define BIGINT_SECURE_OPS_HPP

#include "core.hpp"
#include "range.hpp"

namespace bigint {

// returns lhs if b==false, else rhs:
inline u32 select(u32 lhs, u32 rhs, bool b) {
	const auto mask1 = u32{u32{0} - u32{b}};
	const auto mask0 = ~mask1;
	return (lhs bitand mask0) bitor (rhs bitand mask1);
}

void select(limb_span out, limb_view lhs, limb_view rhs, bool b);

} //namespace bigint


#endif
