#ifndef BIGINT_CORE_HPP
#define BIGINT_CORE_HPP

#include <cstdint>

namespace bigint {

using u32 = std::uint32_t;
using u64 = std::uint64_t;

inline u32 narrow(u64 n) { return static_cast<u32>(n); }

inline std::size_t bits_to_limbs(std::size_t bits) { return (bits + 31u) / 32u; }

} // namespace bigint

#endif
