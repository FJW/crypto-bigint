#ifndef BIGINT_PARSING_HPP
#define BIGINT_PARSING_HPP

#include <optional>
#include <string_view>

#include "core.hpp"
#include "range.hpp"

namespace bigint {

constexpr bool is_hex(char c) { return ('0' <= c and c <= '9') or ('a' <= c and c <= 'f'); }

inline constexpr std::optional<std::size_t> count_limbs(std::string_view sv) {
	if (sv.size() < 3 or sv.substr(0, 2) != "0x" or sv[2] == '\'') {
		return std::nullopt;
	}
	auto bits = std::size_t{};
	for (auto c : sv.substr(2)) {
		if (is_hex(c)) {
			bits += 4u;
		} else if (c != '\'') {
			return std::nullopt;
		}
	}
	return {(bits + 31u) / 32u};
}

bool parse_bigint(limb_span out, std::string_view in);

} // namespace bigint

#endif
