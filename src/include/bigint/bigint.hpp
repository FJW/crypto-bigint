#ifndef BIGINT_BIGINT_HPP
#define BIGINT_BIGINT_HPP

#include <array>
#include <cassert>
#include <cstdint>
#include <initializer_list>
#include <ostream>
#include <utility>
#include <vector>

#include "arithmetics.hpp"
#include "core.hpp"
#include "parsing.hpp"
#include "utils.hpp"

namespace bigint {

using u32 = std::uint32_t;
using u64 = std::uint64_t;

class bigint {
public:
	explicit bigint(std::size_t bits = 0u);
	explicit bigint(std::vector<u32> raw);
	explicit bigint(std::vector<u32> raw, std::size_t bits);
	explicit bigint(std::string_view hex);

	bigint& operator+=(const bigint& other);
	bigint& operator-=(const bigint& other);
	bigint& operator*=(const bigint& other);
	bigint& operator%=(const bigint& mod);
	bigint& operator/=(const bigint& mod);

	bigint& operator<<=(std::size_t);

	limb_view get_view() const { return {m_data}; }
	limb_span get_span() { return {m_data}; }

	std::size_t bits() const { return m_bits; }

	void update_bits() { m_bits = count_bits(get_view()); }
	bool accurate_bits() const { return m_bits == count_bits(get_view()); }

private:
	std::vector<u32> m_data;
	std::size_t m_bits;
};

bool operator==(const bigint& lhs, const bigint& rhs);
inline bool operator!=(const bigint& lhs, const bigint& rhs) { return not(lhs == rhs); }

bool operator<(const bigint& lhs, const bigint& rhs);
inline bool operator>(const bigint& lhs, const bigint& rhs) { return rhs < lhs; }
inline bool operator<=(const bigint& lhs, const bigint& rhs) { return not(rhs < lhs); }
inline bool operator>=(const bigint& lhs, const bigint& rhs) { return not(lhs < rhs); }

bigint operator+(const bigint& lhs, const bigint& rhs);
bigint operator-(const bigint& lhs, const bigint& rhs);
bigint operator*(const bigint& lhs, const bigint& rhs);
bigint operator%(bigint lhs, const bigint& mod);
bigint operator/(bigint lhs, const bigint& mod);
bigint operator<<(bigint lhs, std::size_t n);

bigint pow(const bigint& b, const bigint& e, const bigint& m);

std::ostream& operator<<(std::ostream&, const bigint&);

} // namespace bigint

template <char... C>
bigint::bigint operator""_bi() {
	static constexpr const auto raw = bigint::impl::to_char_array<C...>();
	constexpr const auto raw_sv = std::string_view{raw.data(), raw.size()};
	constexpr const auto limb_count = bigint::count_limbs(raw_sv);
	static_assert(limb_count != std::nullopt, "literal must be a valid hex-string");
	return bigint::bigint{{raw_sv}};
}

#endif
