#ifndef BIGINT_UTILS_HPP
#define BIGINT_UTILS_HPP

namespace bigint::impl {
template <char... C>
constexpr std::array<char, sizeof...(C)> to_char_array() {
	return {{C...}};
}
} // namespace bigint::impl

#endif
