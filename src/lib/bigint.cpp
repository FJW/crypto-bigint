#include <bigint/bigint.hpp>

#include <algorithm>
#include <cassert>
#include <iomanip>
#include <limits>
#include <stdexcept>
#include <tuple>

namespace bigint {

bigint::bigint(std::size_t bits) : m_data(bits_to_limbs(bits)), m_bits{bits} {}

bigint::bigint(std::vector<u32> raw)
        : m_data{std::move(raw)}, m_bits{count_bits(limb_view{m_data})} {
	assert(m_data.size() != 0);
}

bigint::bigint(std::vector<u32> raw, std::size_t bits) : m_data{std::move(raw)}, m_bits{bits} {
	assert(m_data.size() != 0);
}

bigint::bigint(std::string_view hex) {
	const auto limb_count = count_limbs(hex);
	if (limb_count == std::nullopt) {
		throw std::runtime_error{"invalid hex-string"};
	}
	auto vec = std::vector<u32>(*limb_count);
	const auto success = parse_bigint(vec, hex);
	if (not success) {
		throw std::runtime_error{"parsing error"};
	}
	m_data = std::move(vec);
	m_bits = count_bits(limb_view{m_data});
	if (m_bits == 0u) {
		m_data.clear();
	}
}

bool operator==(const bigint& lhs, const bigint& rhs) {
	return compare(lhs.get_view(), rhs.get_view()) == order::equal;
}
bool operator<(const bigint& lhs, const bigint& rhs) {
	return compare(lhs.get_view(), rhs.get_view()) == order::less;
}

bigint operator+(const bigint& lhs, const bigint& rhs) {
	auto vec = std::vector<u32>(std::max(lhs.get_view().size(), rhs.get_view().size()) + 1);
	const auto carry = add(vec, lhs.get_view(), rhs.get_view());
	vec.back() = carry;
	return bigint{std::move(vec), std::max(lhs.bits(), rhs.bits()) + 1u};
}

bigint& bigint::operator+=(const bigint& other) {
	const auto old_size = get_view().size();
	const auto new_bits = std::max(bits(), other.bits()) + 1u;
	const auto resize_needed = (new_bits > m_data.size() * 32u);
	if (resize_needed) {
		m_data.resize((new_bits + 31u) / 32u);
	}
	const auto carry = add(m_data, limb_view{m_data.data(), old_size}, other.get_view());
	if (resize_needed) {
		m_data.back() = carry;
	} else {
		assert(carry == 0u);
	}
	m_bits = new_bits;
	return *this;
}

bigint operator-(const bigint& lhs, const bigint& rhs) {
	assert(lhs >= rhs);
	auto vec = std::vector<u32>(lhs.get_view().size());
	[[maybe_unused]] const auto carry = sub(vec, lhs.get_view(), rhs.get_view());
	assert(carry == 0);
	return bigint{std::move(vec)};
}

bigint& bigint::operator-=(const bigint& other) {
	assert(*this >= other);
	[[maybe_unused]] const auto carry = sub(get_span(), get_view(), other.get_view());
	assert(carry == 0);
	return *this;
}

bigint operator*(const bigint& lhs, const bigint& rhs) {
	const auto out_bits = lhs.bits() + rhs.bits();
	const auto out_size = bits_to_limbs(out_bits);
	auto vec = std::vector<u32>(out_size);
	mul(vec, lhs.get_view(), rhs.get_view());
	return bigint{std::move(vec), out_bits};
}

bigint& bigint::operator*=(const bigint& other) {
	const auto out_bits = bits() + other.bits();
	const auto out_size = bits_to_limbs(out_bits);
	// we copy this in case that m_data has enough capacity, which
	// will decrease the size of the necessary allocation
	const auto tmp = m_data;
	m_data.resize(out_size);
	mul(m_data, tmp, other.get_view());
	m_bits = out_bits;
	return *this;
}

bigint& bigint::operator%=(const bigint& m) {
	assert(m.accurate_bits());
	if (bits() < m.bits()) {
		return *this;
	}
	auto scratch_space = std::vector<u32>(m_data.size());
	mod(get_span(), m.get_view(), m.bits(), limb_span(scratch_space));
	m_data.resize(m.get_view().size());
	m_bits = m.bits();
	return *this;
}

bigint operator%(bigint lhs, const bigint& mod) {
	assert(mod.accurate_bits());
	lhs %= mod;
	return lhs;
}

bigint& bigint::operator/=(const bigint& m) {
	*this = (*this / m);
	return *this;
}

bigint operator/(bigint lhs, const bigint& mod) {
	const auto lhs_size = lhs.get_view().size();
	auto scratch_space = std::vector<u32>(lhs_size);
	auto out = std::vector<u32>(lhs_size);
	div_mod(out, lhs.get_span(), mod.get_view(), scratch_space);
	return bigint{std::move(out)};
}

bigint pow(const bigint& b, const bigint& e, const bigint& m) {
	assert(m.accurate_bits());
	const auto res_bits = m.bits();
	const auto res_limbs = m.get_view().size();
	auto vec = std::vector<u32>(res_limbs);
	auto scratch = std::vector<u32>(res_limbs * 7u);
	pow(limb_span{vec}, b.get_view(), e.get_view(), m.get_view(), res_bits, scratch);
	return bigint{std::move(vec), res_bits};
}

bigint& bigint::operator<<=(std::size_t n) {
	const auto old_size = get_view().size();
	const auto new_size = old_size + ((n + 31u) / 32u);
	m_data.resize(new_size);
	lshift(m_data, {m_data.data(), old_size}, n);
	m_bits += n;
	return *this;
}

bigint operator<<(bigint lhs, std::size_t n) {
	const auto old_size = lhs.get_view().size();
	const auto new_size = old_size + ((n + 31u) / 32u);
	auto vec = std::vector<u32>(new_size);
	lshift(vec, lhs.get_view(), n);
	return bigint{std::move(vec), lhs.bits() + n};
}

std::ostream& operator<<(std::ostream& s, const bigint& i) {
	const auto v = i.get_view();
	s << "0x";
	for (auto j = v.size(); j-- > 0u;) {
		s << std::hex << std::setw(8) << std::setfill('0') << v[j];
	}
	return s;
}

} // namespace bigint
