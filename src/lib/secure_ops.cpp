#include <bigint/secure_ops.hpp>

#include <cassert>

namespace bigint {

void select(limb_span out, limb_view lhs, limb_view rhs, bool b) {
	assert(lhs.size() == rhs.size());
	assert(out.size() == lhs.size());
	for (auto i = std::size_t{}; i < out.size(); ++i) {
		out[i] = select(lhs[i], rhs[i], b);
	}
}

} // namespace bigint
