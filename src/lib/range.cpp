#include <bigint/range.hpp>

#include <ostream>

namespace bigint {

std::ostream& operator<<(std::ostream& out, limb_view v) {
	out << '[' << v[0];
	for (auto i = 1u; i < v.size(); ++i) {
		out << ", " << v[i];
	}
	return out << ']';
}

std::size_t count_bits(limb_view v) {
	for (auto i = v.size(); i-- > 0u;) {
		if (auto tmp = v[i]; tmp != 0u) {
			auto ret = i * 32u + 1u;
			while (tmp >>= 1u) {
				++ret;
			}
			return ret;
		}
	}
	return 0u;
}

} // namespace bigint
