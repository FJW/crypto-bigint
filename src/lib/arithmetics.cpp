#include <bigint/arithmetics.hpp>
#include <bigint/secure_ops.hpp>

#include <cassert>
#include <exception>
#include <tuple>

#include <iostream>

namespace bigint {

std::ostream& operator<<(std::ostream& out, order o) {
	switch (o) {
	case order::less:
		return out << "'<'";
	case order::equal:
		return out << "'='";
	case order::greater:
		return out << "'>'";
	}
	assert(false && "this is supposed to be unreachable");
	std::terminate();
}

namespace {
// The compiler will HOPEFULLY keep this branchless:
std::tuple<bool, bool> compare(u32 lhs, u32 rhs, bool less, bool equal) {
	const auto new_less = (lhs < rhs);
	const auto new_equal = (lhs == rhs);
	const auto l = u32{less} bitor (u32{equal} bitand u32{new_less});
	const auto e = u32{equal} bitand u32{new_equal};
	return {static_cast<bool>(l), static_cast<bool>(e)};
}
} // anonymous namespace

order compare(limb_view lhs, limb_view rhs) {
	auto equal = true;
	auto less = false;
	const auto shared_limbs = std::min(lhs.size(), rhs.size());
	for (auto i = lhs.size(); i-- > shared_limbs;) {
		std::tie(less, equal) = compare(lhs[i], 0u, less, equal);
	}
	for (auto i = rhs.size(); i-- > shared_limbs;) {
		std::tie(less, equal) = compare(0u, rhs[i], less, equal);
	}
	for (auto i = shared_limbs; i-- > 0u;) {
		std::tie(less, equal) = compare(lhs[i], rhs[i], less, equal);
	}
	return static_cast<order>(u32{equal} << 1u bitor u32{less});
}

namespace {
std::tuple<u32, u32> half_add(u32 lhs, u32 carry) {
	const auto tmp = u64{u64{lhs} + u64{carry}};
	return {narrow(tmp >> 32u), narrow(tmp)};
}
std::tuple<u32, u32> full_add(u32 lhs, u32 rhs, u32 carry) {
	const auto tmp = u64{u64{lhs} + u64{rhs} + u64{carry}};
	return {narrow(tmp >> 32u), narrow(tmp)};
}
} // anonymous namespace

u32 add(limb_span out, limb_view lhs, limb_view rhs) {
	assert(out.size() >= std::max(lhs.size(), rhs.size()));
	if (lhs.size() < rhs.size()) {
		return add(out, rhs, lhs);
	}
	auto carry = u32{};
	for (auto i = std::size_t{}; i < rhs.size(); ++i) {
		std::tie(carry, out[i]) = full_add(lhs[i], rhs[i], carry);
	}
	for (auto i = rhs.size(); i < lhs.size(); ++i) {
		std::tie(carry, out[i]) = half_add(lhs[i], carry);
	}
	return carry;
}

namespace {
std::tuple<u32, u32> half_sub(u32 lhs, u32 rhs) { return {lhs < rhs, lhs - rhs}; }

std::tuple<u32, u32> full_sub(u32 lhs, u32 rhs, u32 carry) {
	assert(carry <= 1u);
	const auto new_carry = u64{lhs} < u64{rhs} + u64{carry};
	return {u32{new_carry}, lhs - (rhs + carry)};
}
} // anonymous namespace

u32 sub(limb_span out, limb_view lhs, limb_view rhs) {
	// requires lhs >= rhs
	assert(out.size() >= lhs.size());
	assert(lhs.size() >= rhs.size());
	auto carry = u32{};
	for (auto i = std::size_t{}; i < rhs.size(); ++i) {
		std::tie(carry, out[i]) = full_sub(lhs[i], rhs[i], carry);
	}
	for (auto i = rhs.size(); i < lhs.size(); ++i) {
		std::tie(carry, out[i]) = half_sub(lhs[i], carry);
	}
	return carry;
}

namespace {
std::tuple<u32, u32> half_sub_if(u32 lhs, u32 rhs, bool b) {
	const auto mask = u32{0 - u32{b}};
	return {lhs < rhs, lhs - (rhs bitand mask)};
}

std::tuple<u32, u32> full_sub_if(u32 lhs, u32 rhs, u32 carry, bool b) {
	assert(carry <= 1u);
	const auto mask = u32{0 - u32{b}};
	const auto new_carry = u64{lhs} < u64{rhs} + u64{carry};
	return {u32{new_carry}, lhs - ((rhs + carry) bitand mask)};
}
} // anonymous namespace
void sub_if(limb_span out, limb_view lhs, limb_view rhs, bool b) {
	// requires lhs >= rhs
	assert(out.size() >= lhs.size());
	assert(lhs.size() >= rhs.size());
	auto carry = u32{};
	for (auto i = std::size_t{}; i < rhs.size(); ++i) {
		std::tie(carry, out[i]) = full_sub_if(lhs[i], rhs[i], carry, b);
	}
	for (auto i = rhs.size(); i < lhs.size(); ++i) {
		std::tie(carry, out[i]) = half_sub_if(lhs[i], carry, b);
	}
}

void lshift(limb_span out, limb_view in, std::size_t n) {
	const auto to_pd = [](std::size_t i) { return static_cast<std::ptrdiff_t>(i); };
	const auto limb_shift = n / 32u;
	const auto bit_shift = n % 32u;
	std::move_backward(in.begin(), in.end(), out.begin() + to_pd(in.size() + limb_shift));
	std::fill(out.begin(), out.begin() + to_pd(limb_shift), u32{});
	if (bit_shift == 0u) {
		return;
	}
	auto carry = u32{};
	// TODO: out.size() works, but is excessive
	for (auto i = limb_shift; i < out.size(); ++i) {
		const auto next_carry = out[i] >> (32u - bit_shift);
		out[i] = (out[i] << bit_shift) bitor carry;
		carry = next_carry;
	}
	// assert(carry == 0);
}

void div_2(limb_span inout) {
	auto carry = u32{};
	for (auto i = inout.size(); i-- > 0u;) {
		const auto new_carry = ((inout[i] bitand 1u) << 31u);
		inout[i] = (inout[i] >> 1u) bitor carry;
		carry = new_carry;
	}
}

bool mul_2(limb_span inout) {
	auto carry = false;
	for (auto i = std::size_t{}; i < inout.size(); ++i) {
		const auto new_carry = inout[i] >> 31u;
		inout[i] = (inout[i] << 1u) + carry;
		carry = static_cast<bool>(new_carry);
	}
	return carry;
}

namespace {
std::tuple<u32, u32> mul(u32 lhs, u32 rhs) {
	const auto tmp = u64{u64{lhs} * u64{rhs}};
	return {narrow(tmp >> 32u), narrow(tmp)};
}
} // anonymous namespace

void mul(limb_span out, limb_view lhs, limb_view rhs) {
	if (rhs.size() > lhs.size()) {
		return mul(out, rhs, lhs);
	}
	assert(out.size() >= lhs.size() + rhs.size());
	std::fill(out.begin(), out.end(), u32{});
	for (auto i = std::size_t{}; i < rhs.size(); ++i) {
		auto carry = u32{};
		for (auto j = std::size_t{}; j < lhs.size(); ++j) {
			const auto [mul_carry, mul_res] = mul(lhs[j], rhs[i]);
			const auto [add_carry, digit] = full_add(mul_res, out[i + j], carry);
			// cannot overflow because mul_carry <= b-2 and add_carry <= 1 => sum <= b-1
			carry = mul_carry + add_carry;
			out[i + j] = digit;
		}
		for (auto j = i + lhs.size(); j < lhs.size() + rhs.size(); ++j) {
			std::tie(carry, out[j]) = half_add(out[j], carry);
		}
		assert(carry == 0);
	}
}

void mod(limb_span inout, limb_view m, std::size_t m_bits, limb_span scratch) {
	assert(scratch.size() >= inout.size());
	assert(m_bits <= m.size() * 32u);
	if (inout.size() < m.size()) {
		return;
	}
	assert(inout.size() * 32u > m_bits);
	const auto mod_bits = m_bits != 0 ? m_bits : m.size() * 32u;
	const auto shift = (inout.size() * 32u) - mod_bits;
	lshift(scratch, m, shift);
	for (auto i = std::size_t{}; i <= shift; ++i) {
		const auto b = compare(inout, scratch) == order::greater;
		sub_if(inout, inout, scratch, b);
		div_2(scratch);
	}
}

void div_mod(limb_span div_out, limb_span inout, limb_view rhs, limb_span scratch) {
	assert(div_out.size() >= inout.size()); // Is this really necessary as such?
	assert(scratch.size() == inout.size());
	const auto lhs_bits = count_bits(inout);
	const auto rhs_bits = count_bits(rhs);
	std::fill(div_out.begin(), div_out.end(), 0u);
	const auto rounds = lhs_bits - rhs_bits + 1u;
	lshift(scratch, rhs, rounds - 1u);
	for (auto i = 0u; i < rounds; ++i) {
		auto tmp = inout >= scratch;
		mul_2(div_out);
		div_out[0] += tmp;
		sub_if(inout, inout, scratch, tmp);
		div_2(scratch);
	}
}

void mul_m(limb_span out, limb_view lhs, limb_view rhs, limb_view m, std::size_t mod_bits,
           limb_span scratch) {
	assert(lhs.size() <= m.size());
	assert(rhs.size() <= m.size());
	assert(scratch.size() >= 4 * m.size());
	assert(out.size() >= m.size());
	auto [prod, scratch_mod] = split(scratch, 2 * m.size(), 2 * m.size());
	mul(prod, lhs, rhs);
	mod(prod, m, mod_bits, scratch_mod);
	std::copy(prod.begin(), prod.begin() + m.size(), out.begin());
}

void pow(limb_span out, limb_view base, limb_view exp, limb_view m, std::size_t mod_bits,
         limb_span scratch) {
	const auto mod_size = m.size();
	assert(out.size() >= mod_size);
	assert(mod_bits <= mod_size * 32u);
	assert(scratch.size() >= 7 * mod_size);

	std::fill(scratch.begin(), scratch.end(), 0);
	auto [b, scratch_m, one, factor] =
	        split(scratch, mod_size, 4 * mod_size, mod_size, mod_size);

	std::fill(out.begin(), out.end(), 0u);
	out[0] = 1;
	out = out.splice(mod_size);

	one[0] = 1;

	std::copy(base.begin(), base.end(), b.begin());
	const auto exp_bits = exp.size() * 32u;
	for (auto i = std::size_t{}; i < exp_bits; ++i) {
		select(factor, one, b, exp.get_bit(i));
		mul_m(out, out, factor, m, mod_bits, scratch_m);
		mul_m(b, b, b, m, mod_bits, scratch_m);
	}
}

} // namespace bigint
