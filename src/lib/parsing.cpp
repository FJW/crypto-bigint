#include <bigint/parsing.hpp>

#include <cassert>

namespace bigint {

namespace {
u32 hex_to_u32(char c) {
	assert(is_hex(c));
	if ('0' <= c and c <= '9') {
		return static_cast<u32>(c - '0');
	} else {
		return static_cast<u32>(10 + c - 'a');
	}
}
} // anonymous namespace

bool parse_bigint(limb_span out, std::string_view in) {
	assert(count_limbs(in) != std::nullopt);
	assert(out.size() >= *count_limbs(in));
	auto i = std::size_t{};
	auto j = 0u;
	auto tmp = u32{};
	for (auto k = in.size(); k-- > 2u;) {
		const auto c = in[k];
		if (is_hex(c)) {
			tmp |= hex_to_u32(c) << (4 * j++);
			if (j == 8u) {
				j = 0u;
				out[i++] = tmp;
				tmp = 0u;
			}
		} else if (c == '\'') {
			continue;
		} else {
			return false;
		}
	}
	if (j != 0) {
		out[i++] = tmp;
	}
	return true;
}

} // namespace bigint
